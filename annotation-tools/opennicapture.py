import subprocess
import numpy as np
import os
import fcntl
import scipy.misc as smisc
import atexit
import time
import utils
import numpy.ctypeslib as ctlib

class OpenniCapture(object):
    def __init__(self, isasus=True):
        import openni
        try:
            ctx = openni.Context()

            # Initialize context object
            config_file = "/home/vikasdhi/SamplesConfig.xml"
            assert os.path.exists(config_file)
            #ctx.init_from_xml_file(config_file)
            ctx.init()

            # depth.fps(30)

            # Create a DepthGenerator node
            depth = openni.DepthGenerator()


            # Create a RGBGenerator
            imagegen = openni.ImageGenerator()
            #depth.set_depth_registration(imagegen);

            # from opencv, properties specific to asus
            # if isasus:
            #     imagegen.set_int_property("InputFormat", 1) # XN_IO_IMAGE_FORMAT_YUV422
            #     imagegen.set_pixel_format(openni.PixelFormat.PIXEL_FORMAT_RGB24)
            #     depth.set_int_property("RegistrationType", 1)

            depth.create(ctx)
            imagegen.create(ctx)

            #ctx.set_global_mirror(True)
            #print(ctx.get_global_mirror())
            #imagegen.set_resolution_preset(openni.RES_QVGA)

            # Make it start generating data
            ctx.start_generating_all()
        except openni.OpenNIError, err:
            print("OpenNI found an error:", err.status_string)
            print("try killall XnSensorServer")
            raise err
        else:
            self.ctx = ctx
            self.depthgen = depth
            self.rgbgen = imagegen

    def grab(self):
        try:
            self.ctx.wait_and_update_all()
        except OpenNIError, err:
            print("Failed updating data", err.status_string)

    def focal_length(self):
        pixel_size = self.depthgen.get_real_property("ZPPS")
        depth_focal_length_SXGA = self.depthgen.get_int_property("ZPD")
        depth_focal_length = depth_focal_length_SXGA / pixel_size
        output_x_resolution = self.depthgen.metadata.res[0]
        XN_SXGA_X_RES = 1280.
        scale = output_x_resolution / XN_SXGA_X_RES
        #if not depth_registered:
        return depth_focal_length * scale

    def depthres(self):
        xres, yres = self.depthgen.metadata.res
        return xres, yres

    def intrinsic_matrix(self):
        f = self.focal_length()
        xres, yres = self.depthres()
        K = np.array([[f, 0, xres/2],
                     [0, f, yres/2],
                     [0, 0, 1]])
        return K

    def depth(self):
        """Depth in mm"""
        depthMap = self.depthgen.get_raw_depth_map()
        import openni
        assert openni.PixelFormat.PIXEL_FORMAT_GRAYSCALE_16_BIT ==  \
                self.depthgen.metadata.pixel_format, \
               "Unknown depth pixel format"
        depthshape = self.depthgen.metadata.res[::-1]
        depthnp = np.fromstring(depthMap, dtype=np.uint16).reshape(depthshape)
        return depthnp

    def rgb(self):
        """Does not works: RGB image"""
        #assert False, "Does not works"
        import openni
        assert openni.PixelFormat.PIXEL_FORMAT_RGB24 == \
                self.rgbgen.metadata.pixel_format, \
                "Unknown rgb pixel format"
        imagegen = self.rgbgen
        rgbshape =  imagegen.metadata.res[::-1] + [3]
        rgbMap = imagegen.get_raw_image_map()
        rgbnp = np.fromstring(rgbMap, dtype=np.uint8).reshape(
                                rgbshape)
        #rgbTuple = imagegen.get_tuple_image_map()
        #rgbnp = np.asarray(rgbTuple).reshape(rgbshape)

        return rgbnp

    def points(self):
        K = self.intrinsic_matrix()
        xres, yres = self.depthres()
        rows, cols = np.indices((yres, xres))
        Kinv = np.linalg.inv(K)
        depthnp = self.depth()
        pts_hom = np.vstack((rows.reshape(1, -1),
                            cols.reshape(1, -1),
                            np.ones((1, len(rows.ravel())))))
        pts3d = np.dot(Kinv, pts_hom)
        pts3d = pts3d / pts3d[2, :] * depthnp.ravel()
        return pts3d.T

class OpenniCapDevShm(object):
    lock_file =     "/dev/shm/opennicap.lock"
    depth_file =    "/dev/shm/opennicap_depth.png"
    rgb_file =      "/dev/shm/opennicap_rgb_image.png"
    metadata_file = "/dev/shm/opennicap_metadata.txt"
    def __init__(self):
        self._rgb = None
        self._depth = None
        self._focal_length_inverse = None
        self._lock_fd = None
        self._child_pid = None

    def init(self):
        subprocess.call(["killall", "XnSensorServer"])
        child_process = subprocess.Popen(["opennicap_shm_io", "330"])
        def killchild():
            print("kill %d" % child_process.pid)
            child_process.kill()

        atexit.register(killchild)
        while (not os.path.exists(self.lock_file)):
            print("Lock file do not exists")
            time.sleep(1)
        self._lock_fd = open(self.lock_file)

        self.grab()
        while not self._depth.shape:
            print("Invalid depth file")
            time.sleep(1)
            self.grab()

    def grab(self):
        try:
            fcntl.lockf(self._lock_fd, fcntl.LOCK_SH)
            self._rgb = smisc.imread(self.rgb_file)
            self._depth = smisc.imread(self.depth_file)
            metadata = open(self.metadata_file)
            self._focal_length_inverse  = float(metadata.next().strip())
        except Exception, e:
            print("Error occured", e)
        finally:
            fcntl.lockf(self._lock_fd, fcntl.LOCK_UN)
    def rgb(self):
        return self._rgb

    def depth(self):
        # convert mm to m
        return self._depth / 1000.

    def focal_length(self):
        return 1/self._focal_length_inverse

    def depthres(self):
        rows, cols = self._depth.shape
        xres, yres = cols, rows
        return xres, yres

    def intrinsic_matrix(self):
        f = self.focal_length()
        xres, yres = self.depthres()
        K = np.array([[f, 0, xres/2],
                     [0, f, yres/2],
                     [0, 0, 1]])
        return K

    def points(self):
        K = self.intrinsic_matrix()
        depthnp = self.depth()
        return utils.depth2xyzs(depthnp, K)

class DepthSource(object):
    """Uses openni2"""
    def __init__(self, width, height):
        self.height = height
        self.width = width

    def __enter__(self):
        from primesense import openni2
        openni2.initialize()     # can also accept the path of the OpenNI redistribution
        dev = openni2.Device.open_any()
        sinfo = dev.get_sensor_info(openni2.SENSOR_DEPTH)
        filtered_modes = [m for m in sinfo.videoModes
                          if (m.resolutionX == self.width
                              and m.resolutionY == self.height
                              and m.pixelFormat ==
                              openni2.PIXEL_FORMAT_DEPTH_1_MM)]
        if not len(filtered_modes):
            raise ValueError( ("No mode with specified (width, height)=(%d, %d)."
                               + " Available modes are : ") % (self.width,
                                                               self.height) +
                             str(sinfo.videoModes))
        depth_stream = dev.create_depth_stream()
        depth_stream.set_video_mode(filtered_modes[-1])
        depth_stream.start()
        self.depth_stream = depth_stream

        # prepare RGB
        rgb_info = dev.get_sensor_info(openni2.SENSOR_COLOR)
        rgb_filtered_modes = [m for m in rgb_info.videoModes
                          if (m.resolutionX == self.width
                              and m.resolutionY == self.height
                              and m.pixelFormat ==
                              openni2.PIXEL_FORMAT_RGB888)]
        if not len(rgb_filtered_modes):
            raise ValueError( ("No mode with specified (width, height)=(%d, %d)."
                               + " Available modes are : ") % (self.width,
                                                               self.height) +
                             str(rgb_info.videoModes))
        rgb_stream = dev.create_color_stream()
        rgb_stream.set_video_mode(rgb_filtered_modes[-1])
        rgb_stream.start()
        self.rgb_stream = rgb_stream

        self.dev = dev

        return self

    def K(self):
        vm = self.depth_stream.get_video_mode()
        fovx = self.depth_stream.get_horizontal_fov()
        fovy = self.depth_stream.get_vertical_fov()
        fx = (vm.resolutionX / 2.) / np.tan(fovx / 2.)
        fy = (vm.resolutionY / 2.) / np.tan(fovy / 2.)
        centerx = vm.resolutionX / 2.
        centery = vm.resolutionY / 2.
        return np.array([[fx,  0, centerx],
                         [ 0, fy, centery],
                         [ 0,  0,       1]])

    def rgb(self):
        rgb_stream = self.rgb_stream
        frame = rgb_stream.read_frame()
        frame_data = frame.get_buffer_as_triplet()
        img = np.frombuffer(frame_data, dtype=np.uint8)
        img.shape = (frame.height, frame.width, 3)
        img = img[:, ::-1] # returns correct image don't know why
        return img

    def depth(self):
        depth_stream = self.depth_stream
        frame = depth_stream.read_frame()
        frame_data = frame.get_buffer_as_uint16()
        #img = ctlib.as_array(frame_data, shape=(frame.width, frame.height))
        img = np.frombuffer(frame_data, dtype=np.uint16)
        img = img.reshape(frame.height, frame.width)
        img = img[:, ::-1] # returns correct image don't know why
        return img
        #return img * 0.001 # mm -> m

    def __exit__(self, type, value, traceback):
        self.rgb_stream.stop()
        self.depth_stream.stop()
        self.dev.close()
        from primesense import openni2
        openni2.unload()

def clip_depth(pts_hom, flatdepth, clip):
    nonzerodepthfilter = (flatdepth != 0) # invalid depth
    clipfilter = (flatdepth < 4.0)
    filter = nonzerodepthfilter & clipfilter
    assert np.sum(filter) > 0
    nonzerodepth = flatdepth[filter]
    pts_hom = pts_hom[:, filter]
    return pts_hom, nonzerodepth

def depthtopoints(depthnp, K, depthfilter):
    Kinv = np.linalg.inv(K)
    nrows, ncols = depthnp.shape
    rows, cols = np.indices((nrows, ncols))
    pts_hom = np.vstack((rows.reshape(1, -1),
                         cols.reshape(1, -1),
                         np.ones((1, len(rows.ravel())))))
    flatdepth = depthnp.ravel()
    pts_hom, nonzerodepth = depthfilter(pts_hom, flatdepth)
    pts3d = np.dot(Kinv, pts_hom)
    pts3d = pts3d / pts3d[2, :] * nonzerodepth
    return pts3d.T

class PointSource(object):
    def __init__(self, depthsource=DepthSource(640, 480), clip_range=4.0):
        self.depthsource = depthsource
        self.clip_range = clip_range

    def __enter__(self):
        self.depthsource.__enter__()
        return self

    def points(self):
        depth = self.depthsource.depth()
        depth = depth * 0.001 # mm -> m
        nonzerodepthfilter = (depth != 0) # invalid depth
        clipfilter = (depth < self.clip_range)
        filter = nonzerodepthfilter & clipfilter
        rgb = self.depthsource.rgb()
        return utils.depth2xyzs(depth, self.depthsource.K(),
                                color=rgb,
                                depth_filter=filter), depth, rgb

    def depth(self):
        return self.depthsource.depth()

    def __exit__(self, *args):
        self.depthsource.__exit__(*args)

if __name__ == '__main__':
    import visualiser
    from tvtk.api import tvtk
    ds = DepthSource(640, 480)
    import cv2
    # show point cloud
    with PointSource(ds) as cap:
        print("Point source working")
        polydata = tvtk.PolyData()
        xyz_rgbs, depth, rgb = cap.points()
        print("Got first point cloud")
        visualiser.show_points(xyz_rgbs[:, :3],
                               colors=xyz_rgbs[:, 3:] / 255.0,
                               polydata=polydata)
        def timer_callback(vtkRenWinInt, event):
            xyz_rgbs, depth, rgb = cap.points()
            cv2.imshow('c', depth)
            bgr = cv2.cvtColor(rgb, cv2.COLOR_RGB2BGR)
            cv2.imshow('d', bgr)
            cv2.waitKey(1)
            visualiser.show_points(xyz_rgbs[:, :3],
                                   colors=xyz_rgbs[:, 3:] / 255.0,
                                   polydata=polydata)
            vtkRenWinInt.GetRenderWindow().Render()
        print("Visualizer initialized")
        visualiser.show_window(timer_callback=timer_callback)
    #cap.grab()
    #rgb = cap.rgb()
    #import scipy.misc as smisc
    #smisc.imshow(rgb)

