#!/usr/bin/env python

import urllib2
import tarfile
import os
import subprocess

DOWNLOAD_URL = "https://www.dropbox.com/s/21ynz1r1pb3hgw4/bimodal_sparse_data.tar.gz?dl=1"

LOCALFILENAME = 'bimodal_sparse_data.tgz'
CURL_CMD = ["curl", "-L",
            "--header", 'User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:31.0) Gecko/20100101 Firefox/31.0',
            "--header", 'DNT: 1',
            "--header", 'Connection: keep-alive',
            "-b", "/tmp/curl.cookie",
            DOWNLOAD_URL,
        "-o", LOCALFILENAME]

def scriptdir():
    return os.path.dirname(__file__) or './'

def download_untar(data_url, fname, parentdir):
    localfname = os.path.join(parentdir, fname)
    if not os.path.exists(localfname) or os.stat(localfname)[6] == 0:
        print ' '.join(["'" + cmd + "'" if " " in cmd else cmd for cmd in CURL_CMD])
        subprocess.call(CURL_CMD)

        # url = urllib2.urlopen(data_url)
        # localFile = open(localfname, 'w')
        # print 'Downloading test data:', url.geturl(), ' to ', parentdir + fname
        # localFile.write(url.read())
        # localFile.close()
    
    # extract tar ball
    print("tar xzf %s" % localfname)
    subprocess.call(["tar", "xzf", localfname])
    # tar = tarfile.open(localfname, mode='r:gz')
    # tar.extractall(path=parentdir)
    # tar.close()

    return os.path.splitext(localfname)[0]


if __name__ == '__main__':
    download_untar(DOWNLOAD_URL, LOCALFILENAME, scriptdir())
