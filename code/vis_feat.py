import Image # Python Imaging library
import code # For debugging
import numpy as np # Numerical operations
import cv2 as cv # opencv
import math
import os
import matplotlib.pyplot as plt # For image display
import string # For string splitting 

def visual_feat(im_path):
    # Reading original image
    im_org = Image.open(im_path)
    # Converting to black and white
    im = im_org.convert('1')#im.show()
    # Count number of pixels
    width,height = im.size
    # Load contents of the image
    mat = im.load()
    # Creating feature 2D array
    sp_size = 16 # Split size in horizontal and vertical direction
    feat = np.zeros((width/sp_size,height/sp_size))
    for i in range(width/sp_size):
        for j in range(height/sp_size):
            # Now we have a small image
            num_fg_pix = 0 # Count number of fg pixels
            for y in range(i*sp_size,(i+1)*sp_size):
                for x in range(j*sp_size,(j+1)*sp_size):
                    # If this element will add anything
                    if mat[x,y]>0:
                        num_fg_pix = num_fg_pix+mat[x,y]
            feat[i,j] = (num_fg_pix*1.0)/255
    #plt.imshow(feat,cmap=plt.cm.hot)
    feat_val = np.zeros((width/sp_size)*(height/sp_size)+3)
    # Assigning shape features
    feat_val[0:feat_val.shape[0]-3] = feat.ravel()
    # feat_val stores the feature value,getting the RGB values
    a = im_org.getextrema()
    # Assigning last 3 elements the values of RGB vectors
    feat_val[-3:] = np.array([a[0][1],a[1][1],a[2][1]])
    return feat_val

'''
This function evaluates shape featuers using fourier histograms
Inputs are : im_dir - The directory location that contains the background image bg_rgb.png
im_name : Name of current image also stored in im_dir directory
'''
def vis_fourier(im_dir,im_name):
    
    # Read the current image
    im_curr = cv.imread(os.path.join(im_dir,im_name))

    '''
    # This was neccesary to segment Kinect images
    # Read the background image
    im_bg = cv.imread(os.path.join(im_dir,"bg_1_rgb.png"))
    # Converting it to RGB from current BGR
    # im_curr = cv.cvtColor(im_curr,cv.COLOR_BGR2RGB)
    # Subtracting the image
    im_diff = cv.absdiff(im_bg,im_curr)
    # Converting the image to gray scale
    im_diff_gray = cv.cvtColor(im_diff,cv.COLOR_RGB2GRAY)
    cv.imshow('Diff Image',im_diff_gray)
    cv.waitKey(-1)
    # Thresholding this image to get contours
    # Dont really know what is the right value to use as threshold
    dummy,im_diff_gray = cv.threshold(im_diff_gray,128,255,0)
    '''
    # We will use this method on data_shapes which are images on a table top with shadows
    # Idea is to convert it to HSV space and threshold in the RGB space
    im = cv.cvtColor(im_curr,cv.COLOR_RGB2HSV)
    # Threshold is currently color based, should be made independent but doesn;t matter for now
    fname = string.split(im_name,'_')
    if (fname[0] == 'blue'):
        if fname[1] == 'rectangle':
            mult = 0.5
        else:
            mult = 0.6    
    elif (fname[0] == 'green'):
        mult = 0.6
    elif (fname[0] == 'yellow'):
        if fname[1]=='square':
            mult = 0.95
        else:
            mult = 0.9
    else:
        mult = 0.9
    dummy,im_diff_gray = cv.threshold(im[:,:,1],mult*255,255,cv.THRESH_BINARY)

    # Fill contours in image
    contours, heirarchy = cv.findContours(im_diff_gray,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    if len(contours)==1:
        contour_id = 0
    elif len(contours)>1:
        # Finding the biggest contour and hoping that it is the contour we want
        contour_id = big_cont(contours)
    else:
        # This means no contours were found
        return []
    # Approximate freeman chain for this contour
    cc,start = freeman_chain(contours[contour_id])
    # Plot the contour obtained by freeman chain
    # plot_chain_code(cc,start,im_curr)
    # Obtain features to be used for classification
    coeff,a0,c0 = fourier_approx(cc,10,100,True)
    # np.asarray(coeff).reshape(-1)
    # Getting the RGB values of pixels inside the contour
    mask = np.zeros(im_diff_gray.shape,np.uint8)
    cv.drawContours(mask,contours,contour_id,255,-1)
    av_rgb = cv.mean(im_curr,mask=mask)
    '''
    # Using a boundng rectangle is not a good idea
    # Getting a bounding rectangle for the contour
    en_rect = cv.boundingRect(contours[contour_id])
    av_rgb = cv.mean(im_curr[en_rect[1]:en_rect[1]+en_rect[3],en_rect[0]:en_rect[0]+en_rect[2],:])
    # Improve on the method for computing average rgb values
    # http://stackoverflow.com/questions/17936510/how-to-find-average-intensity-of-opencv-contour-in-realtime
    '''
    feat_val = np.zeros((coeff.shape[0]*coeff.shape[1]+3))
    feat_val[0:coeff.shape[0]*coeff.shape[1]] = np.asarray(coeff.reshape(-1))
    feat_val[-3:] = np.array([av_rgb[0]/255,av_rgb[1]/255,av_rgb[2]/255])
    # Get points on the fourier curve
    '''    
    pts = plot_fourier(np.reshape(feat_val[0:-3],(10,4)),0,0)
    plt.plot(pts[:,0],pts[:,1])
    plt.show()
     
    cv.drawContours(im_curr,contours[contour_id],-1,(255,255,0),4)
    cv.imshow('Diff Image',im_curr)
    cv.waitKey(-1)
    '''
 
    return feat_val

'''
Find the biggest contour
input: list of contours
output: contour id of biggest contour
'''
def big_cont(contours):
    id = 0
    for i in range(len(contours)):
        if cv.arcLength(contours[i],True)>cv.arcLength(contours[id],True):
            id = i
    return id

'''
This function will convert a contour to freeman chain code
Needs a opencv contour as input
Return chain code and starting point as output
'''
def freeman_chain(contour):
    # Convert the contour to a numpy 2D array
    contour = np.asarray(contour)
    np_cont = np.zeros((contour.shape[0],2))
    np_cont[:,0] = contour[:,0,0]
    np_cont[:,1] = contour[:,0,1]
    # Getting dx and dy by creating a circularly shifted array
    np_cont_s = np.zeros((contour.shape[0],2))
    np_cont_s[-1,:] = np_cont[0,:]
    np_cont_s[0:-1,:] = np_cont[1:,:]
    delta  = np_cont_s - np_cont
    # Mapping it to chain values
    idx = 3*delta[:,1]+delta[:,0]+5
    # Creating a dictionary
    cm = {1:5,2:6,3:7,4:4,6:0,7:3,8:2,9:1}
    cc = np.zeros((contour.shape[0]))
    for i in range(delta.shape[0]):
        cc[i] = cm[idx[i]]

    return cc,np_cont[0,:]

''' 
This function plots the chain code and expects an input image, chain code and start point as input

'''
def plot_chain_code(cc,start,im):
    # Creating a dictionary for movement using chain code
    mv = {0:[1,0],1:[1,1],2:[0,1],3:[-1,1],4:[-1,0],5:[-1,-1],6:[0,-1],7:[1,-1]}
    # Starting with the start point
    for i in range(cc.shape[0]):
        # Plot lines on image
        # Start point
        st = np.asarray(start, dtype=np.int)
        # End point
        ed = st+mv[cc[i]]
        cv.line(im,tuple(st),tuple(ed),(255,255,0))
        # Moving start point
        start = ed
    #cv.imshow('Freeman Approx',im)
    #cv.waitKey(-1)

''' 
This function calculates fourier coefficients of a chain code
Inputs: cc - chain code,
        n - number of harmonic elements
        m - number of points for reconstruction
        normalization
'''
def fourier_approx(cc,n = 10,m=100,normalization=True):
    # Calculate fourier coefficients
    # Four coefficients are a_n,b_n,c_n,d_n - Elliptic Fourier features
    # Calculate traversal time for the chain assuming uniform unit speed
    # Calculate travel distance as well
    t = np.zeros((cc.shape[0])) # Stores the time at each chain code instant
    x = np.zeros((cc.shape[0])) #Stores the projection on x axis
    y = np.zeros((cc.shape[0])) # Stores the projection on y axis
    for i in range(cc.shape[0]):
        if i>0:
            t[i] = t[i-1]+1+((np.sqrt(2)-1)/2)*(1-((-1)**cc[i]))
            x[i] = x[i-1]+sign(6-cc[i])*sign(2-cc[i])
            y[i] = y[i-1]+sign(4-cc[i])*sign(cc[i])
        else:
            # For first time interval
            t[i] = 1+((np.sqrt(2)-1)/2)*(1-((-1)**cc[i]))
            x[i] = sign(6-cc[i])*sign(2-cc[i])
            y[i] = sign(4-cc[i])*sign(cc[i])

    coeff = np.zeros((n,4))
    for i in range(n):
        coeff[i,:] = calc_harmonic(cc,i+1,t,x,y)
    # Calculate DC components
    if normalization:
        # Do normalization
        a0 = 0;c0 = 0 # DC components are made zero
        # Normalizing for rotation
        theta1 = 0.5*math.atan(2*((coeff[0,0]*coeff[0,1]+coeff[0,2]*coeff[0,3])/(coeff[0,0]**2+coeff[0,2]**2-coeff[0,1]**2-coeff[0,3]**2)))
        costh1 = np.cos(theta1);sinth1 = np.sin(theta1)
        a_star_1 = costh1*coeff[0,0]+sinth1*coeff[0,1]
        c_star_1 = costh1*coeff[0,2]+sinth1*coeff[0,3]
        psi1 = math.atan(c_star_1/a_star_1)
        E = np.sqrt(a_star_1**2+c_star_1**2)
        cospsi1 = np.cos(psi1)
        sinpsi1 = np.sin(psi1)
        # Normalization of coefficients
        rotmat1 = np.matrix(([cospsi1,sinpsi1],[-sinpsi1,cospsi1]))
        for i in range(coeff.shape[0]):
            normalized = rotmat1*np.matrix(([coeff[i,0],coeff[i,1]],[coeff[i,2],coeff[i,3]]))*np.matrix(([np.cos(theta1*(i+1)),-np.sin(theta1*(i+1))],[np.sin(theta1*(i+1)),np.cos(theta1*(i+1))]))
            coeff[i,:] = (1/E)*np.array([normalized[0,0],normalized[0,1],normalized[1,0],normalized[1,1]])

    else:
        # Calculate DC Components
        a0,c0 = calc_dc(cc,t,x,y)
    plot_fourier(coeff,a0,c0)
    return coeff,a0,c0 

''' 
Plot the results from fourier approximation
Inputs: coeff - Fourier coefficients
        a0,c0 - DC coefficients
        m - Number of points for curve reconstruction
'''
def plot_fourier(coeff,a0,c0,m=100):
    # Stores x and y of the points
    pts = np.zeros((m+1,2))
    for t in range(1,m+1):
        x_ = 0;y_ = 0
        for i in range(coeff.shape[0]):
            x_ = x_+coeff[i,0]*np.cos((2*(i+1)*np.pi*t)/m)+coeff[i,1]*np.sin((2*(i+1)*np.pi*t)/m)
            y_ = y_+coeff[i,2]*np.cos((2*(i+1)*np.pi*t)/m)+coeff[i,3]*np.sin((2*(i+1)*np.pi*t)/m)
        pts[t-1,0] = a0+x_;pts[t-1,1] = c0+y_
    # Closing the curve by writing first point at end as well
    pts[-1,0] = pts[0,0];pts[-1,1] = pts[0,1]
    '''
    l1 = plt.plot(pts[:,0],pts[:,1])
    plt.setp(l1, linewidth=4, color='b')  # line1 is thick and red
    plt.show()
    '''
    return pts
'''
Calculate the DC components of fourier transform
Inputs: same as calc_harmonic except the nth harmonic
'''
def calc_dc(cc,t,x,y):
    a0 = 0
    c0 = 0
    for i in range(cc.shape[0]):
        if i == 0:
            a0 = a0+(x[i]/(2*t[i]))*t[i]*t[i]
            c0 = c0+(y[i]/(2*t[i]))*t[i]*t[i]
        elif i==1:
            a0 = a0+((x[i]-x[i-1])/(2*(t[i]-t[i-1])))*t[i]*t[i]
            c0 = c0+((y[i]-y[i-1])/(2*(t[i]-t[i-1])))*t[i]*t[i]
        else:
            delta_x = x[i]-x[i-1]
            delta_y = y[i]-y[i-1]
            delta_t = t[i] - t[i-1]
            zeta_p = x[i-1]-(delta_x/delta_t)*t[i-1]
            delta_p = y[i-1]-(delta_y/delta_t)*t[i-1]
            a0 = a0+(delta_x/(2*delta_t))*(t[i]**2-t[i-1]**2)+zeta_p*(t[i]-t[i-1])
            c0 = c0+(delta_y/(2*delta_t))*(t[i]**2-t[i-1]**2)+delta_p*(t[i]-t[i-1])
    return a0/t[-1],c0/t[-1]


'''
Calculates ith harmonic coefficients
For details refer Elliptic Fourier features of a closed contour by Kuhl nad Giardina, 1981
Input: cc - chain code
        i - ith coefficient
        t - time taken at each link chain
        x - x projection of distance travelled at each link
        y - y projection of distance travelled at each link
'''
def calc_harmonic(cc,i,t,x,y):
    # Calculating the final time period T
    T = t[-1]
    two_n_pi_T = (2*i*np.pi)/T # Store this value for faster computation
    sigma_a = 0
    sigma_b = 0
    sigma_c = 0
    sigma_d = 0
    for p in range(cc.shape[0]):
        if (p>1):
            tp_prev = t[p-1]
            dx = x[p]-x[p-1]
            dy = y[p]-y[p-1]
            dt = t[p]-t[p-1]
        elif (p==1):
            tp_prev = 0
            dx = x[p]-x[p-1]
            dy = y[p]-y[p-1]
            dt = t[p]-t[p-1]
        else:
            tp_prev = 0
            dx = x[p]
            dy = y[p]
            dt = t[p]
        # Calcuating the actual coefficient
        q_x = dx/dt
        q_y = dy/dt
        sigma_a = sigma_a+q_x*(np.cos(two_n_pi_T*t[p])-np.cos(two_n_pi_T*tp_prev))
        sigma_b = sigma_b+q_x*(np.sin(two_n_pi_T*t[p])-np.sin(two_n_pi_T*tp_prev))
        sigma_c = sigma_c+q_y*(np.cos(two_n_pi_T*t[p])-np.cos(two_n_pi_T*tp_prev))
        sigma_d = sigma_d+q_y*(np.sin(two_n_pi_T*t[p])-np.sin(two_n_pi_T*tp_prev))
    # Calculatiing the final coefficients
    coeffs = np.array([sigma_a,sigma_b,sigma_c,sigma_d])
    r = T/(2*np.pi*np.pi*i*i)
    return coeffs*r

'''
Because python doesn't have a sign function :(
'''
def sign(i):
    if (abs(i-0.0)<1e-5):
        return 0
    else:
        return abs(i)/i




if __name__ == '__main__':
    base_dir = '/home/surenkum/work/vision_lang/data/data_shapes/images'
    #nm_file = 'blue_circle_2_rgb.png'
    #vis_fourier(base_dir,nm_file)
    
    # Test all the files in current folder
    for root,dirs,files in os.walk(base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("yellow_triangle")==True):
                print "Processing "+nm_file
                vis_fourier(base_dir,nm_file)

    '''
    cc = np.array([5,4,1,2,3,4,3,0,0,1,0,1,0,0,0,7,7,1,1,0,7,5,4,5,4,5,0,6,5,4,1,3,4,4,4,4,6])
    fourier_approx(cc)
    '''

