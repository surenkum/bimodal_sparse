'''
This code measures the H matrix by measuring cluster similarity between
clustering obtained by clustering in image space and audio space
'''

from sklearn import cluster
import os
import aud_feat
import numpy as np
import vis_feat as vf
from sklearn import metrics
from munkres import Munkres
from utils import datadir


'''
Computer clusters in visual and audio domain and measures similarity of the resulting clusters using
V-measure
Input: base_dir - Directory to find image and audio files
Output: Similarity matrix
'''
def measure_sim(base_dir):
    # Getting the audio and visual features over the dataset
    im_base_dir = os.path.join(base_dir,'images') # Image base directory
    aud_base_dir = os.path.join(base_dir,'audio') # audio data base directory
    audio_mat_1 = []
    audio_mat_2 = []
    visual_mat = []
    for root,dirs,files in os.walk(im_base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("bg")==False):
                print "Reading file"+os.path.join(im_base_dir,nm_file)
                # Getting visual features
                vis_feat = vf.vis_fourier(im_base_dir,nm_file)
                # Reading audio feature separately for first audio (shape) and second audio (color) segment
                audio_feat_1 = aud_feat.audio_pro(os.path.join(aud_base_dir,nm_file[0:-4]+'_shape.wav.mfcc.csv'))
                audio_feat_1 = audio_feat_1.ravel()
                audio_feat_2 = aud_feat.audio_pro(os.path.join(aud_base_dir,nm_file[0:-4]+'_color.wav.mfcc.csv'))
                audio_feat_2 = audio_feat_2.ravel()
                # Checking if audio_mat or video_mat haven't been initialized
                if len(audio_mat_1)==0:
                    audio_mat_1 = audio_feat_1.reshape(-1,1)
                    audio_mat_2 = audio_feat_2.reshape(-1,1)
                    visual_mat = vis_feat.reshape(-1,1)
                else:
                    # Appending the current data to previous data
                    audio_mat_1 = np.append(audio_mat_1,audio_feat_1.reshape(-1,1),1)
                    audio_mat_2 = np.append(audio_mat_2,audio_feat_2.reshape(-1,1),1)
                    visual_mat = np.append(visual_mat,vis_feat.reshape(-1,1),1)

    # Actual clustering
    # km need the matrix to be of shape (n_samples,n_features)
    visual_mat = np.transpose(visual_mat)
    audio_mat_1 = np.transpose(audio_mat_1)
    audio_mat_2 = np.transpose(audio_mat_2)
    km = cluster.KMeans(4) # There are four colors
    vis_color = km.fit_predict(visual_mat[:,-3:]) # Prediction from RGB features
    aud_color_1 = km.fit_predict(audio_mat_1) # Prediction from first audio segments
    aud_color_2 = km.fit_predict(audio_mat_2) # Prediction from second audio segments
    v1 = metrics.v_measure_score(vis_color,aud_color_1) # Distane between clusters
    v2 = metrics.v_measure_score(vis_color,aud_color_2) # Distane between clusters
    km = cluster.KMeans(8) # There are eight shapes
    vis_shape = km.fit_predict(visual_mat[:,:-3]) # Prediction from RGB features
    aud_shape_1 = km.fit_predict(audio_mat_1) # Prediction from first audio segments
    aud_shape_2 = km.fit_predict(audio_mat_2) # Prediction from second audio segments
    s1 = metrics.v_measure_score(vis_shape,aud_shape_1) # Distane between clusters
    s2 = metrics.v_measure_score(vis_shape,aud_shape_2) # Distane between clusters

    return [[-v1,-v2],[-s1,-s2]]

'''
This function does hungarian assignment for assigning modalities between audio and 
visual domains
Inputs: matrix measuring clustering similarity between visual and audio domains using V-measure
Output: Assignment of the variable
'''

def assignment(matrix):
    m = Munkres()
    indexes = m.compute(matrix)
    return indexes



if __name__ == '__main__':
    base_dir = datadir('data_shapes')
    matrix = measure_sim(base_dir) # Get the clustering similarity matrix
    indexes = assignment(matrix) # Get the hungarian assignment
    for index in indexes:
        print 'Visual domain '+str(index[0])+' assigned to audio domain '+str(index[1]) 
