# Used for testing image segmentation from
import os
import cv2


def vis_segment(base_dir,nm_file):
    # Current image file name
    img = cv2.imread(os.path.join(base_dir,nm_file))
    # Converting to HSV image
    im = cv2.cvtColor(img,cv2.COLOR_RGB2HSV)
    ret, thresh = cv2.threshold(im[:,:,1],0.5*255,255,cv2.THRESH_BINARY)
    cv2.imshow('Tresholded',thresh)
    cv2.waitKey(-1)

if __name__ == '__main__':
    base_dir = '/home/surenkum/work/vision_lang/data/data_shapes/images'
    # Test all the files in current folder
    for root,dirs,files in os.walk(base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("green")==True):
                print "Processing "+nm_file
                vis_segment(base_dir,nm_file)
