# This will generate all the splits and generate figues needed for the paper
import os
import string
import glob
import comp_dict_learn as main_alt # Learn compositional mapping
import utils
import main # Learn single mapping
import eval_quantitative
from utils import datadir

'''
This will generate a training and testing text file and test for accuracy
Inputs : base_dir - Base Directory
        sp_type - Testing for can be 'reprod' for reproduction accuracy
        compositional accuracy - 'general'      

'''
def train_test_splits(base_dir,sp_type = 'reprod'):
    c_comp = [];s_comp = [];a_comp = [];im_comp = [] # To store compositional matches in color, shape and both
    c_single = [];s_single = [];a_single = [];im_single = [] # To store single matches in color, shape and both
    im_base_dir = os.path.join(base_dir,'images')
    # Check if we are testing for reproduction accuracy
    if sp_type == 'reprod':
        # Dividing into three sets for reproduction accuray
        num_sp = 3
        for i in range(num_sp):
            # Create a file with details of training files
            train_f = open(datadir('train_set.txt'),'w')
            # Create a file with details of testing files
            test_f = open(datadir('test_set.txt'),'w')
            # Save two sets for training and third set for testing
            for root,dirs,files in os.walk(im_base_dir):
                for nm_file in files:
                    if nm_file.endswith("png") and (nm_file.startswith("bg")==False):
                        # Reading all the image files which are not background files
                        f_sp = string.split(nm_file,'_')
                        # Check the number of file
                        if ((int(f_sp[2])%num_sp) == i):
                            # Testing set file
                            test_f.write(nm_file);test_f.write('\n')
                        else:
                            # Training test file
                            train_f.write(nm_file);train_f.write('\n')
            
            # Close the files
            train_f.close()
            test_f.close()
            # Training using the compositional dictionary model
            train_file = datadir('train_set.txt')
            (D1,D2,visual_mat) = main_alt.learn_comp_dict(base_dir,train_file)
            # Training using the single dictionary model
            (D,visual_mat) = main.learn_single_dict(base_dir,train_file)
            # Testing for accuracy on testing set
            test_file = datadir('test_set.txt')
            # We want to write these results
            write_dir_comp = datadir('../results/reprod/comp/'+str(i+1))
            write_dir_single = datadir('../results/reprod/single/'+str(i+1))
            if not os.path.exists(write_dir_comp):
                os.makedirs(write_dir_comp) # Create the directory if it doesn't exist
            if not os.path.exists(write_dir_single):
                os.makedirs(write_dir_single) # Create the directory
            # Test using the single dictionary
            (c_mat_single,s_mat_single,a_mat_single,im_mat_single) = utils.test_audio(base_dir,test_file,D,visual_mat.shape[0],1,True,write_dir_single)
            c_single.append(c_mat_single);s_single.append(s_mat_single);a_single.append(a_mat_single);im_single.append(im_mat_single)
            # Test using the coupled dictionary
            (c_mat_comp,s_mat_comp,a_mat_comp,im_mat_comp) = utils.test_audio_alt(base_dir,test_file,D1,D2,visual_mat[:-3,:].shape[0],visual_mat[-3:,:].shape[0],1,True,write_dir_comp)
            c_comp.append(c_mat_comp);s_comp.append(s_mat_comp);a_comp.append(a_mat_comp);im_comp.append(im_mat_comp)


    else:
        # Testing for generalization accuracy
        colors = ['blue','red','green','yellow']
        shapes = ['rhombus','circle','halfcircle','rectangle','square','triangle','trapezium','hexagon']
        # Pick an color and shape and investigate if its possible to create a generalization problem
        for shape in shapes:
            # For each color generate a test and training file set
            for color in colors:
                if (color== 'blue' and shape == 'rhombus')or (color== 'red' and shape == 'trapezium') or (color== 'yellow' and shape == 'hexagon'):
                    pass
                else:
                    # Create a file with details of training files
                    train_f = open(datadir('train_set.txt'),'w')
                    # Create a file with details of testing files
                    test_f = open(datadir('test_set.txt'),'w')

                    # Check if the current color and shape have the image
                    # We can only generate examples for generalization if there is nothing 
                    if (len(glob.glob(im_base_dir+'/*green''_'+shape+'*.png'))==0):

                        # Save all the images with audio for training and generated audio for testing
                        for root,dirs,files in os.walk(im_base_dir):
                            for nm_file in files:
                                if nm_file.endswith("png") and (nm_file.startswith("bg")==False):
                                    # Reading all the image files which are not background files
                                    f_sp = string.split(nm_file,'_')
                                    # Training test file
                                    train_f.write(nm_file);train_f.write('\n')
                        # Writing testing file data
                        num_ex = 5 # Same as the dummy audio files we generated
                        for i in range(num_ex):
                            test_f.write(color+'_'+shape+'_'+str(i+1)+'_rgb.png');test_f.write('\n')

                    else:
                        # Create a file with details of training files
                        train_f = open(datadir('train_set.txt'),'w')
                        # Create a file with details of testing files
                        test_f = open(datadir('test_set.txt'),'w')
                        # Save two sets for training and third set for testing
                        for root,dirs,files in os.walk(im_base_dir):
                            for nm_file in files:
                                if nm_file.endswith("png") and (nm_file.startswith("bg")==False):
                                    # Reading all the image files which are not background files
                                    f_sp = string.split(nm_file,'_')
                                    # Check the number of file
                                    if (f_sp[0] == color) and (f_sp[1] == shape):
                                        # Testing set file
                                        test_f.write(nm_file);test_f.write('\n')
                                    else:
                                        # Training test file
                                        train_f.write(nm_file);train_f.write('\n')

                    # Close the files
                    train_f.close()
                    test_f.close()

                    # Training using the compositional dictionary model
                    train_file = datadir('train_set.txt')
                    (D1,D2,visual_mat) = main_alt.learn_comp_dict(base_dir,train_file)
                    # Training using the single dictionary model
                    (D,visual_mat) = main.learn_single_dict(base_dir,train_file)
                    # Testing for accuracy on testing set
                    test_file = datadir('test_set.txt')
                    # We want to write these results
                    write_dir_comp = os.path.join(os.getcwd(),'results/generalization/comp/'+color+'_'+shape)
                    write_dir_single = os.path.join(os.getcwd(),'results/generalization/single/'+color+'_'+shape)
                    if not os.path.exists(write_dir_comp):
                        os.makedirs(write_dir_comp) # Create the directory if it doesn't exist
                    if not os.path.exists(write_dir_single):
                        os.makedirs(write_dir_single) # Create the directory

                    utils.test_audio_alt(base_dir,test_file,D1,D2,visual_mat[:-3,:].shape[0],visual_mat[-3:,:].shape[0],1,True,write_dir_comp)
                    utils.test_audio(base_dir,test_file,D,visual_mat.shape[0],1,True,write_dir_single)
    return (c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single)
                    
if __name__ == '__main__':
    # Test image directory
    base_dir = os.path.join(utils.datadir(), 'data_shapes')
    #train_test_splits(base_dir,'generalization') # For generalization accuracy
    (c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single) = train_test_splits(base_dir,'reprod') # For reproduction accuracy
    eval_quantitative.gen_plots(c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single)

