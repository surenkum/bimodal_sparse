# Utility file for testing and display
import os
import vis_feat as vf # For visual features
import aud_feat # For audio features
import numpy as np
import spams
import matplotlib.pyplot as plt
import cv2 as cv # Using opencv for plotting
import code
import Queue
import string

def test_audio(base_dir,aud_file,D,vdim,coeff,fourier=False,write_dir = []):
    c_mat = []; s_mat = [];a_mat = []; # To store color, shape and all other matches
    im_mat = []; # To store the images that are processed
    aud_base_dir = os.path.join(base_dir,'audio') # audio data base directory
    im_base_dir = os.path.join(base_dir,'images') # audio data base directory
    if aud_file==[]:
        # Test all the files in current folder
        for root,dirs,files in os.walk(base_dir):
            for nm_file in files:
                if nm_file.endswith(".wav") and (nm_file.startswith("bg")==False):
                    # Process each wave file
                    single_test_audio(os.path.join(base_dir,nm_file+'.mfcc.csv'),D,vdim,coeff,fourier,base_dir)

    else:
        # Read a file and process the test images from the file
        f = open(aud_file,'r') # This really has the image visual data

        for nm_file in f:
            # nm_file is the name of an image file
            im_mat.append(nm_file) # Appending the name of images
            # Corresponding audio file
            nm_file = nm_file[:-8]+'audio.wav'
            # Process each wave file
            vis_feat = single_test_audio(os.path.join(aud_base_dir,nm_file+'.mfcc.csv'),D,vdim,coeff,fourier,base_dir,write_dir)
            # Testing it using K nearest neighbors
            dist_shape,dist_rgb,im_names = knn_metric(vis_feat,im_base_dir,'single')
            [c_match,s_match,a_match] = knn_evaluate(nm_file,dist_shape,dist_rgb,im_names)
            print c_match,s_match,a_match
            c_mat.append(c_match);s_mat.append(s_match);a_mat.append(a_match)
            # Write a composite image
            index_color = np.argsort(dist_rgb)
            index_shapes = np.argsort(dist_shape)
            im_names_color = []; im_names_shape = []
            for i in range(4):
                im_names_color.append(im_names[index_color[i]])
                im_names_shape.append(im_names[index_shapes[i]])
            im_comp = gen_composite(im_names_color,im_base_dir)
            cv.imwrite(os.path.join(write_dir,nm_file[:-4]+'comp_color.png'),im_comp)
            im_comp = gen_composite(im_names_shape,im_base_dir)
            cv.imwrite(os.path.join(write_dir,nm_file[:-4]+'comp_shape.png'),im_comp)
    
    return (c_mat,s_mat,a_mat,im_mat)


def single_test_audio(aud_file,D,vdim,coeff,fourier,base_dir,write_dir = []):
    # Read a audio file and get its audio feature
    audio_feat = aud_feat.audio_pro(aud_file)
    audio_feat = audio_feat.ravel()
    # For using in basis match pursuit algorithm
    audio_feat = np.asfortranarray(audio_feat.reshape(-1,1),dtype=float)
    # Dictionary for just the audio features
    audio_D = np.asfortranarray(D[vdim:,:],dtype=float)
    alpha = spams.omp(audio_feat,audio_D,lambda1 = coeff)

    # Multiplying with visual basis to get visual data
    visual_feat = (D[0:vdim,:])*alpha
    print "Processed audio file "+aud_file
    if fourier:
        # Visual features are fourier features
        # Follow appropriate fourier picture
        # Get points on the fourier curve
        pts = vf.plot_fourier(np.reshape(visual_feat[0:-3],(10,4)),0,0)
        # Create a new blank image
        blank_im = 255*np.ones((480,640,3),np.uint8)
        pts[:,0] = pts[:,0]*80; pts[:,1] = pts[:,1]*80; pts[:,0] = pts[:,0]+320;pts[:,1] = pts[:,1]+240
        col_val = np.array([visual_feat[-3]*255,visual_feat[-2]*255,visual_feat[-1]*255],dtype=np.uint8)
        cv.fillPoly(blank_im,np.array([pts],dtype=np.int32),color = (map(int,col_val[:,0])))
        #cv.imshow("Output Image",blank_im);cv.waitKey(10)
        if not write_dir:
            # Write_dir is empty
            pass
        else:
            fname = string.rsplit(aud_file,'/')
            cv.imwrite(os.path.join(write_dir,fname[-1][:-19]+'.png'),blank_im)
        '''
        im_names =  knn_visual(visual_feat,base_dir)
        im_comp = gen_composite(im_names,base_dir)
        cv.imshow("8 Nearest Image",im_comp);cv.waitKey(-1)
        cv.imwrite("composite_im.png",im_comp)
        '''
    else:
        # Displaying this visual data
        im = np.reshape(visual_feat[0:-3],(32,32))
        '''
        # Trying to get the color of the images
        # Dont know why this array gets the values it does
        im = im*(im>0.81)
        #Making an complete image with color
        img = np.zeros((im.shape[0],im.shape[1],3),dtype=float)
        img[:,:,0] = im*((visual_feat[-3]*255)/np.sum(visual_feat[-3:]))
        img[:,:,1] = im*((visual_feat[-2]*255)/np.sum(visual_feat[-3:]))
        img[:,:,2] = im*((visual_feat[-1]*255)/np.sum(visual_feat[-3:]))
        #code.interact(local=locals())
        plt.imshow(img)
        '''
        plt.imshow(im,cmap=plt.cm.hot)
        plt.show()

    return visual_feat[:,0]

''' 
This function will test the audio for our compositional sparse representation
'''
def test_audio_alt(base_dir,aud_file,D1,D2,vdim1,vdim2,coeff,fourier=True,write_dir = []):
    c_mat = []; s_mat = [];a_mat = []; # To store color, shape and all other matches
    im_mat = [] # To store the name of images
    # Getting image and audio directories
    im_base_dir = os.path.join(base_dir,'images') # audio data base directory
    aud_base_dir = os.path.join(base_dir,'audio') # audio data base directory
    if aud_file==[]:
        # Test all the files in current folder
        for root,dirs,files in os.walk(aud_base_dir):
            for nm_file in files:
                if nm_file.endswith("_shape.wav") and (nm_file.startswith("bg")==False):
                    # Process each wave file
                    vis_feat = single_test_audio_alt(os.path.join(aud_base_dir,nm_file+'.mfcc.csv'),D1,D2,vdim1,vdim2,coeff,fourier,base_dir,write_dir)
                    
                    # Testing it using K nearest neighbors
                    dist_shape,dist_rgb,im_names = knn_metric(vis_feat,im_base_dir)
                    [c_match,s_match,a_match] = knn_evaluate(nm_file,dist_shape,dist_rgb,im_names)
                    print c_match,s_match,a_match
                    c_mat.append(c_match);s_mat.append(s_match);a_mat.append(a_match)
                    

    else:
        # Read a file and process the test images from the file
        f = open(aud_file,'r') # This really has the image visual data
        for nm_file in f:
            # To store the name of processes image file
            im_mat.append(nm_file)
            # nm_file is the name of an image file
            nm_file = nm_file[:-5]+'_shape.wav'
            # Process each wave file
            vis_feat = single_test_audio_alt(os.path.join(aud_base_dir,nm_file+'.mfcc.csv'),D1,D2,vdim1,vdim2,coeff,fourier,base_dir,write_dir)
            
            # Testing it using K nearest neighbors
            dist_shape,dist_rgb,im_names = knn_metric(vis_feat,im_base_dir)
            [c_match,s_match,a_match] = knn_evaluate(nm_file,dist_shape,dist_rgb,im_names)
            print c_match,s_match,a_match
            c_mat.append(c_match);s_mat.append(s_match);a_mat.append(a_match)
            # Write a composite image
            index_color = np.argsort(dist_rgb)
            index_shapes = np.argsort(dist_shape)
            im_names_color = []; im_names_shape = []
            for i in range(4):
                im_names_color.append(im_names[index_color[i]])
                im_names_shape.append(im_names[index_shapes[i]])
            im_comp = gen_composite(im_names_color,im_base_dir)
            cv.imwrite(os.path.join(write_dir,nm_file[:-4]+'comp_color.png'),im_comp)
            im_comp = gen_composite(im_names_shape,im_base_dir)
            cv.imwrite(os.path.join(write_dir,nm_file[:-4]+'comp_shape.png'),im_comp)


    return (c_mat,s_mat,a_mat,im_mat)
    


'''
For testing a single audio file with compositional sparse learning
'''
def single_test_audio_alt(aud_file,D1,D2,vdim1,vdim2,coeff,fourier,base_dir=[],write_dir = []):
    # Finding the other part of this audio file
    aud_dir = os.path.dirname(aud_file)
    fnames = string.split(os.path.basename(aud_file),'.')
    dummy = fnames[0]
    # We came across the color part
    if dummy[-5:] == 'color':
        aud_file_1 = os.path.join(aud_dir, dummy[:-5]+'shape.wav.mfcc.csv')
        aud_file_2 = aud_file
    else:
        aud_file_1 = aud_file
        aud_file_2 = os.path.join(aud_dir, dummy[:-5]+'color.wav.mfcc.csv')

    
    # Read a audio file and get its audio feature
    audio_feat_1 = aud_feat.audio_pro(aud_file_1) # Shape audio features
    audio_feat_1 = audio_feat_1.ravel()
    audio_feat_2 = aud_feat.audio_pro(aud_file_2) # Color audio features
    audio_feat_2 = audio_feat_2.ravel()
    # For using in basis match pursuit algorithm
    audio_feat_1 = np.asfortranarray(audio_feat_1.reshape(-1,1),dtype=float)
    audio_feat_2 = np.asfortranarray(audio_feat_2.reshape(-1,1),dtype=float)
    # Dictionary for just the audio features
    audio_D_1 = np.asfortranarray(D1[vdim1:,:],dtype=float)
    alpha_1 = spams.omp(audio_feat_1,audio_D_1,lambda1 = coeff)
    audio_D_2 = np.asfortranarray(D2[vdim2:,:],dtype=float)
    alpha_2 = spams.omp(audio_feat_2,audio_D_2,lambda1 = coeff)

    # Multiplying with visual basis to get visual data
    visual_feat_1 = (D1[0:vdim1,:])*alpha_1
    visual_feat_2 = (D2[0:vdim2,:])*alpha_2

    # Getting all the visual feats for the audio files
    vis_feat = np.zeros((visual_feat_1.shape[0]+visual_feat_2.shape[0]))
    vis_feat[0:visual_feat_1.shape[0]] = visual_feat_1[:,0]
    vis_feat[visual_feat_1.shape[0]:] = visual_feat_2[:,0]

    print "Processed audio file "+aud_file

    
    if fourier:
        # Visual features are fourier features
        # Follow appropriate fourier picture
        # Get points on the fourier curve
        pts = vf.plot_fourier(np.reshape(visual_feat_1,(10,4)),0,0)
        # Create a new blank image
        blank_im = 255*np.ones((480,640,3),np.uint8)
        pts[:,0] = pts[:,0]*80; pts[:,1] = pts[:,1]*80; pts[:,0] = pts[:,0]+320;pts[:,1] = pts[:,1]+240
        col_val = np.array([visual_feat_2[0]*255,visual_feat_2[1]*255,visual_feat_2[2]*255],dtype=np.uint8)
        cv.fillPoly(blank_im,np.array([pts],dtype=np.int32),color = (map(int,col_val[:,0])))
        if not write_dir:
            # Write_dir is empty
            pass
        else:
            fname = string.rsplit(aud_file,'/')
            cv.imwrite(os.path.join(write_dir,fname[-1][:-13]+'.png'),blank_im)
        #cv.imshow("Output Image",blank_im);cv.waitKey(-1)
        
        '''
        # K nearest neighbours part
        im_names =  knn_visual(visual_feat,base_dir)
        im_comp = gen_composite(im_names,base_dir)
        cv.imshow("8 Nearest Image",im_comp);cv.waitKey(-1)
        cv.imwrite("composite_im.png",im_comp)
        '''
        
    else:
        # Displaying this visual data
        im = np.reshape(visual_feat[0:-3],(32,32))
        # Trying to get the color of the images
        # Dont know why this array gets the values it does
        im = im*(im>0.81)
        #Making an complete image with color
        img = np.zeros((im.shape[0],im.shape[1],3),dtype=float)
        img[:,:,0] = im*((visual_feat[-3]*255)/np.sum(visual_feat[-3:]))
        img[:,:,1] = im*((visual_feat[-2]*255)/np.sum(visual_feat[-3:]))
        img[:,:,2] = im*((visual_feat[-1]*255)/np.sum(visual_feat[-3:]))
        #code.interact(local=locals())
        plt.imshow(img)
        plt.imshow(im,cmap=plt.cm.hot)
        plt.show()

    return vis_feat 


'''
This function will find the nearest visual neighbours from the test set based on feature value 
predicted by the sparse learning
Inputs: test_feat - test_feat
        gt_dir - ground truth directory where all the images are located
        k - number of neighbours required
        base_dir - base directory where all the images are stored
'''
def knn_visual(test_feat,base_dir,k=8):
    max_dist = 100 # Just a big value
    dist_vals = max_dist*np.ones((k)) # stores distance values for the files stored in im_names
    #im_names = [] # This will store the name of the image files corresponding to the distance measured
    # Processing all the files that are 
    im_names = Queue.PriorityQueue(maxsize=k)
    for root,dirs,files in os.walk(base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("bg")==False):
                # Get current feature value
                vis_feat = vf.vis_fourier(base_dir,nm_file)
                # Measure distance
                dist = dist_metric(vis_feat,test_feat)

                if im_names.full():
                    neg_max_dist, max_dist_file = im_names.get()
                    max_dist = -neg_max_dist
                    if max_dist < dist:
                        im_names.put_nowait((neg_max_dist, max_dist_file))
                    else:
                        im_names.put_nowait((-dist, nm_file))
                else:
                    im_names.put_nowait((-dist, nm_file))
                # Check if the current values need to be changed
                # if dist<dist_vals[0]:
                #     # Updated k nearest neighbours
                #     # Pop out the zeroth distance value
                #     dist_vals[0:-1] = dist_vals[1:]; dist_vals[-1] = dist

    im_names_list = []
    while not im_names.empty():
        im_names_list.append(im_names.get()[1])
    return im_names_list

'''
This is a faster version of knn_visual uses same inputs
test_feat - test visual feature
base_dir - image base directory
k - number of nearest neighbors
n - Indicated the number of shape features to use for distance metric
Its improtant because low frequency components bit the shape the best
'''
def knn_metric(test_feat,base_dir,k=8,n=10,method = 'comp'):

    dist_shape = [] # To store the distance values for the shape part
    dist_rgb = [] # To evaluate RGB values closeness
    im_names = [] # This will store the name of the image files corresponding to the distance measured
    # Processing all the image files
    for root,dirs,files in os.walk(base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("bg")==False):
                # Get current feature value
                vis_feat = vf.vis_fourier(base_dir,nm_file)
                # Measure distance
                #dist_shape.append(dist_metric(vis_feat[:-3],test_feat[:-3])) # Shape feature distance
                # Instead of using all the elements, lets just use first few elements
                # Sometimes coefficients might be positive or negative version of the original features
                
                # To avoid division by zero
                if np.abs(vis_feat[0])<1e-12 or np.abs(test_feat[0])<1e-12:
                    dist = np.min([dist_metric(vis_feat[0:n],-test_feat[0:n]),dist_metric(vis_feat[0:n],test_feat[0:n])])
                else:
                    dist = dist_metric(vis_feat[0:n]/vis_feat[0],test_feat[0:n]/test_feat[0])
                dist_shape.append(dist) # Shape feature distance
                # Dont know why this is happening
                if method == 'single':
                    dist_rgb.append(dist_metric(np.array((vis_feat[-1],vis_feat[-2],vis_feat[-3])),test_feat[-3:]))
                else:
                    dist_rgb.append(dist_metric(vis_feat[-3:],test_feat[-3:])) # Color feature distance
                # Updating the image array
                im_names.append(nm_file)
    # Sorting the array
    return dist_shape,dist_rgb,im_names

'''
Check the rank of neighbour in color and shape
Inputs : Input file name
        dist metric of shape 
        dist metric of color
        image names w.r.t distance metric evaluated
'''
def knn_evaluate(nm_file,dist_shape,dist_rgb,im_names):
    # Converting these distance list to arrays
    dist_shape = np.array(dist_shape)
    dist_rgb = np.array(dist_rgb)
    dist = dist_shape+dist_rgb # Total distance (can be summed because of l1 measure)
    # Getting the color and shape values from input image
    fnames = string.rsplit(nm_file,'_')
    inp_color = fnames[0] # Input image color
    inp_shape = fnames[1] # input image shape
    # Sorting the indices
    index_rgb = np.argsort(dist_rgb) # Sorting RGB distance
    index_shape = np.argsort(dist_shape) # Sorting shape distance
    index = np.argsort(dist) # Sorting the combined distance
    # Finding the color match
    for i in range(dist_shape.shape[0]):
        fnames = string.rsplit(im_names[index_rgb[i]],'_')
        curr_color = fnames[0] # Current image color
        if curr_color == inp_color:
            c_match = i # Color match distance
            break
    # Finding the shape match
    for i in range(dist_shape.shape[0]):
        fnames = string.rsplit(im_names[index_shape[i]],'_')
        curr_shape = fnames[1] # Current image shape
        if curr_shape == inp_shape:
            s_match = i # Shape match found
            break
    # Finding the shape and color match
    for i in range(dist_shape.shape[0]):
        fnames = string.rsplit(im_names[index[i]],'_')
        curr_color = fnames[0] # Current image color
        curr_shape = fnames[1] # Current image shape
        if (curr_shape == inp_shape) and (curr_color == inp_color):
            a_match = i # Shape and color match found
            break
    return c_match,s_match,a_match 



'''
This function measures distance metric between two feature values
Inputs: test_feat - test feature value
        gt_feat - ground truth feature value
'''
def dist_metric(test_feat,gt_feat):
    dist = 0
    for i in range(test_feat.shape[0]):
        dist = dist+abs(test_feat[i]-gt_feat[i]) # l1 norm
    return dist


''' 
Generating composite images from k- nearest neighbours
Will only use 8 images for now
'''
def gen_composite(im_names,base_dir):
    im = np.zeros((480,640,3),np.uint8)
    ind = 0
    for im_name in im_names:
        # Read image
        im_curr = cv.imread(os.path.join(base_dir,im_name))
        # Get row number and col number
        row = ind/2; col = ind - row*2
        ind = ind+1
        # Reduce the size of current image by half and put it
        im[row*240:(row+1)*240,col*320:(col+1)*320,:] = cv.resize(im_curr,(320,240))
    return im

def datadir(subdir=None):
    dd = os.path.join(os.path.abspath(os.path.dirname(__file__)), "../data")
    return dd if subdir is None else os.path.join(dd, subdir)
