import os
import vis_feat as vf # For visual features
import aud_feat # For audio features
import numpy as np
import code
import scipy.io.wavfile # To read .wav files to get audio features
import spams # Sparse Modeling Software v 2.4 by Julien Mairal
import utils

def learn_dict(visual_mat,audio_mat):
    # Appending the data to optimize
    X_org = np.append(visual_mat,audio_mat,0)
    # Doing some more processing
    #meanX = np.mean(X_org,0)
    X = X_org#-np.tile(np.mean(X_org,0),(X_org.shape[0],1))
    X = np.asfortranarray(X,dtype=float)
    #X = np.asfortranarray(X / np.tile(np.sqrt((X * X).sum(axis=0)),(X.shape[0],1)),dtype = float)
    # Using spams dictionary learning
    # Making a default dictionary
    base_dict = np.asfortranarray(np.ones((X.shape[0],50)),dtype=float)
    (D,model) = spams.trainDL(X,return_model=True,D=base_dict,mode=0,lambda1 = 0.15)
    return (D,model)

'''
This function learns a single dictionary using images from a base directory
Optionally one can pass path to a file from which images can be read
'''
def learn_single_dict(base_dir,inp_file = []):
    
    im_base_dir = os.path.join(base_dir,'images') # Image base directory
    aud_base_dir = os.path.join(base_dir,'audio') # audio data base directory
    # Extracted audio data using file
    # yaafe.py -r 44100 -f "mfcc: MFCC blockSize=1024 stepSize=512" -i /home/surenkum/work/vision_lang/data/data2/light_frames/audiofiles.txt

    # Complete audio and video data in these mats
    # Getting the shape and color features in two different dictionaries
    audio_mat = []
    visual_mat = []
    # If there is no input file to read data from
    if not inp_file:
        for root,dirs,files in os.walk(im_base_dir):
            for nm_file in files:
                if nm_file.endswith(".png") and (nm_file.startswith("bg")==False):
                    print "Reading file"+os.path.join(im_base_dir,nm_file)
                    # Getting visual features
                    vis_feat = vf.vis_fourier(im_base_dir,nm_file)
                    # Reading audio feature separately for first audio (shape) and second audio (color) segment
                    audio_feat = aud_feat.audio_pro(os.path.join(aud_base_dir,nm_file[0:-7]+'audio.wav.mfcc.csv'))
                    audio_feat = audio_feat.ravel()
                    # Checking if audio_mat or video_mat haven't been initialized
                    if len(audio_mat)==0:
                        audio_mat = audio_feat.reshape(-1,1)
                        visual_mat = vis_feat.reshape(-1,1)
                    else:
                        # Appending the current data to previous data
                        audio_mat = np.append(audio_mat,audio_feat.reshape(-1,1),1)
                        visual_mat = np.append(visual_mat,vis_feat.reshape(-1,1),1)
    else:
        # There is a input file
        # Read the input file and
        f = open(inp_file,'r') # Open the file in reading mode
        for nm_file in f:
            # Strip the line 
            nm_file = nm_file.rstrip('\n')
            # Line is the name of the image file
            print "Reading file For Training: "+nm_file
            # Getting visual features
            vis_feat = vf.vis_fourier(im_base_dir,nm_file)
            # Reading audio feature separately for first audio (shape) and second audio (color) segment
            audio_feat = aud_feat.audio_pro(os.path.join(aud_base_dir,nm_file[0:-7]+'audio.wav.mfcc.csv'))    

            audio_feat = audio_feat.ravel()
            # Checking if audio_mat or video_mat haven't been initialized
            if len(audio_mat)==0:
                audio_mat = audio_feat.reshape(-1,1)
                visual_mat = vis_feat.reshape(-1,1)
            else:
                # Appending the current data to previous data
                audio_mat = np.append(audio_mat,audio_feat.reshape(-1,1),1)
                visual_mat = np.append(visual_mat,vis_feat.reshape(-1,1),1)

    f.close()
    # Learning two separate dictionaries
    (D,model) = learn_dict(visual_mat,audio_mat) # Just the shape features
    return (D,visual_mat)

if __name__ == "__main__":
    base_dir = "/home/surenkum/work/vision_lang/data/data_shapes"
    (D,visual_mat) = learn_single_dict(base_dir)
    import pdb;pdb.set_trace()
    # Testing the learned dictionary
    # Read a audio file and get its audio feature
    # Testing with a single dictionary
    # utils.test_audio(base_dir,[],D,visual_mat.shape[0],1,True) 
    # Testing with compositional dictionary
    utils.test_audio(base_dir,[],D,visual_mat.shape[0],1,True)



