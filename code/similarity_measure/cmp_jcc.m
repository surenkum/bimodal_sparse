function jcc = cmp_jcc(idx,gt)
%idx,gt is row vector
%%% Real Jaccard's coefficient at time t is calculated 

A_original=pdist2(idx,idx,'hamming');  

A_original=~A_original; A_original=tril(A_original,-1);

A_this_question=A_original;

%%%
no_sim_pairs=size(A_original(A_original(:)==1),1); % We will need this to calculate JCC difference
%%%
gt = double(gt);
A_cluster=pdist2(gt,gt);  A_cluster=~A_cluster; A_cluster=tril(A_cluster,-1);

eva_mat=A_original+A_cluster;
SS=size(find(eva_mat(:)==2));

eva_mat=A_original-A_cluster;
SD=size(find(eva_mat(:)==1));

eva_mat=A_cluster-A_original;
DS=size(find(eva_mat(:)==1));

jcc=SS(1)/(SS(1)+SD(1)+DS(1));% This is the real Jaccard's coeff now