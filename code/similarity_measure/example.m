labelfeatures = [1;1;2;2];
Outputs = [1;2;2;2];
goldla = unique(labelfeatures);%c
resla = unique(Outputs); %k
comatrix = zeros(length(resla),length(goldla)); %k by c
for i = 1:size(comatrix,1)
    co_i = find(Outputs==resla(i));
    for j = 1:size(comatrix,2)
        co_j = find(labelfeatures == goldla(j));
        c_intersect = intersect(co_i,co_j);
        comatrix(i,j) = length(c_intersect);
    end
end
[v,hc,hk,h_ck,h_kc] = calculate_v_measure (comatrix);
RI = v;
[AR,RI1,MI,HI]=valid_RandIndex(Outputs,labelfeatures);
[jcc] = cmp_jcc(Outputs,labelfeatures);
