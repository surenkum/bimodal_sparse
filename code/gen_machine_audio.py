# -*- coding: utf-8 -*-
'''
..module:: gen_machine_sound.py
:synopsis: This module is a program which generates audio files for all the images in a folder
It generates two parts of audio for a single object saying the color and shape in two different parts
'''
from common.espeak_wrapper import ESpeakWrapper
import scipy.io.wavfile
import os
import os.path
import string
import subprocess
import random
import glob

'''
This function takes in a input string and a directory to 
first write audio files corresponding to the color and shape
and then extract features from them as well
'''
def gen_text_features(inp_string,write_dir,inp_voice = 'en-us'):
    # Split the string to write assuming the space between color and shape
    dummy = string.split(inp_string)
    # Generate actual audio streams
    gen_single_sound(dummy[0],dummy[1],write_dir+'/test',inp_voice)
    # Generate a .txt file named audiofiles.txt and write the name of files to that
    f = open('audiofiles.txt','w') # Open files for writing
    f.write(write_dir+'/test_color.wav');f.write('\n')
    f.write(write_dir+'/test_shape.wav');f.write('\n')
    f.close()
    # Run yaafe on these to get features
    subprocess.call(['yaafe.py -r 22050 -f "mfcc: MFCC blockSize=1024 stepSize=512" -i audiofiles.txt'],shell=True)


def gen_single_sound(color,shape,write_dir,inp_voice='en-us'):
    # Color is the color of the incoming object and 
    # shape is the shape of the imaged object
    # write_dir is the directory with path to write files
    # inp_voice is the input language (default english, use hi for Hindi)

    # Get english to Hindi translation dictionary for colors and shapes
    eng_hindi = english_to_hindi()
    audio_ext = '.wav'
    #Sequentially render the different shapes and frames
    esw = ESpeakWrapper(wpm = random.randint(195,200),voice = inp_voice )
    # Generate the color part for the object
    if (inp_voice == 'en-us'):
        rate,audio_arr = esw.get_speech_arr(color)
        audio_file_name = write_dir+"_color" + audio_ext
        #Save out the sound file for just the color part
        scipy.io.wavfile.write(audio_file_name, rate, audio_arr)
        # Generate the shape part for the object
        rate,audio_arr = esw.get_speech_arr(shape)
        #Save out the sound file for just the color part
        audio_file_name = write_dir+"_shape" + audio_ext
        scipy.io.wavfile.write(audio_file_name, rate, audio_arr)
    else:
        # IF the language is Hindi
        rate,audio_arr = esw.get_speech_arr(eng_hindi[color])
        audio_file_name = write_dir+"_color_hi" + audio_ext
        scipy.io.wavfile.write(audio_file_name, rate, audio_arr)
        rate,audio_arr = esw.get_speech_arr(eng_hindi[shape])
        audio_file_name = write_dir+"_shape_hi" + audio_ext
        scipy.io.wavfile.write(audio_file_name, rate, audio_arr)
    

    # Writing the single audio file
    rate,audio_arr = esw.get_speech_arr(color+" "+shape)
    audio_file_name = write_dir[:-3]+'audio'+audio_ext
    scipy.io.wavfile.write(audio_file_name,rate,audio_arr)
    #import pdb;pdb.set_trace()

# Read all the image files in a folder and generate sound for each of them
def gen_directory(base_dir,audio_dir,inp_voice = 'en-us'):
    # Base dir is the directory with all the image files
    #  audio dir is the directory where all the audio files will be written
    for root,dirs,files in os.walk(base_dir):
        for nm_file in files:
            if nm_file.endswith(".png") and (nm_file.startswith("bg")==False):
                print "Reading file"+os.path.join(base_dir,nm_file)
                # Getting the color and shape part of it
                fname = string.split(nm_file,'_',2)
                gen_single_sound(fname[0],fname[1],os.path.join(audio_dir,nm_file[:-4]),inp_voice)

# Generate sounds corresponding to certain shapes and colors as predefined
def gen_color_shape(audio_dir,colors=['blue','green','red','yellow'],shapes = ['rhombus','trapezium','hexagon']):
    num_ex = 5 # Generate num_ex number of example for a particular color and shape
    for color in colors:
        for shape in shapes:
            # Check if no audio files exist for current color and shape
            if (len(glob.glob(audio_dir+'/'+color+'_'+shape+'*.wav'))==0):
                for i in range(num_ex):
                    gen_single_sound(color,shape,os.path.join(audio_dir,color+'_'+shape+'_'+str(i+1)+'_rgb'))

# Hindi translation for english words
def english_to_hindi():
    # Forming the entire dictionary for the words required
    eng_hindi = {'red':'लाल','blue':'नीला','green':'हरा','yellow':'पीला'}
    eng_hindi_shapes = {'circle':'वृत्त','triangle':'त्रिकोण','halfcircle':'अर्धचक्र','hexagon':'षट्भुज','rectangle':'आयत','rhombus':'विषमकोण','square':'वर्ग','trapezium':'समलम्ब'}
    eng_hindi.update(eng_hindi_shapes)
    return eng_hindi


if __name__ == '__main__':
    '''
    This method will generate sounds for all the image files in a folder
    '''
    # Directory where images are located
    base_dir = '/home/surenkum/work/vision_lang/data/data_lang/images'
    # Directory where to write the audio files
    audio_dir = '/home/surenkum/work/vision_lang/data/data_lang/audio'
    #gen_text_features('लाल त्रिकोण',audio_dir,'hi')
    gen_directory(base_dir,audio_dir,'hi') # Generate sounds having corresponding images 
    #gen_color_shape(audio_dir) # Generate sounds for generalization testing
 

