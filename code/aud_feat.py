import csv # For reading csv files
import numpy as np
import utils

'''
Reads a MFCC feature generated to extract top audio segments based on MFCC coefficients
Inputs: Complete path to MFCC .csv file
Output: Audio Feature
'''
def audio_pro(test_file):
    # These files have audio data
    ifile = open(test_file)
    reader = csv.reader(ifile)

    # Creating a audio feature
    audio_data = []
    col_data = np.zeros((1,13))

    rownum = 0
    for row in reader:
        if rownum>5:
            colnum = 0
            for col in row:
                col_data[0,colnum] = col
                colnum += 1
            # Appending the data
            if len(audio_data)==0:
                audio_data = col_data
            else:
                audio_data = np.append(audio_data,col_data,0)
        rownum+=1

    ifile.close()
    # Picking top components from audio data
    b = np.sum(audio_data,axis=1)
    # Doing argument sort over b 
    c = np.argsort(b)

    # Creating audio feature
    top_comps = 15 # Number of top audio components
    audio_feat = np.zeros((top_comps,audio_data.shape[1]))
    for i in range(top_comps):
        audio_feat[i,:] = audio_data[c[-1-i],:]
    return audio_feat

if __name__ =="__main__":
    test_file = utils.datadir("dummy-data/blue_triangle.wav.mfcc.csv")
    audio_pro(test_file)
