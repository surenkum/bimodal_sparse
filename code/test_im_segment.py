import comp_dict_learn as learn
import vis_feat as vf # For visual features
import aud_feat # For getting audio features
import cv2 as cv # Opencv
import numpy as np 
import os
import utils
import pickle

'''
Find multiple segments in an image
inputs : im - image name
'''
def segment_multiple(im):
    # Segment a image with multiple objects
    imdir = '/home/surenkum/work/vision_lang/data/multiple'
    # Test image
    test_im = cv.imread(os.path.join(imdir,im))
    # Test depth
    #test_depth = np.load(os.path.join(imdir,im[:-7]+'depth.npy'))
    im_draw =  cv.cvtColor(test_im,cv.COLOR_BGR2RGB) #np.zeros((test_im.shape),dtype=np.uint8)
    im_rgb =  cv.cvtColor(test_im,cv.COLOR_BGR2RGB) # Store the RGB image
    im = cv.cvtColor(test_im,cv.COLOR_BGR2HSV)
    # Detecting yellow objects
    mult = 0.9
    dummy,im_diff_gray = cv.threshold(im[:,:,1],mult*255,255,cv.THRESH_BINARY)
    contours, heirarchy = cv.findContours(im_diff_gray,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    # Storing the required contours and their centers
    req_contours = []; req_centers = [];
    for i in range(len(contours)):
        if cv.contourArea(contours[i])>1000:
            # Found a legitimate contour
            req_contours.append(contours[i])
            # Estimate the center
            bb = cv.boundingRect(contours[i])
            req_centers.append(np.array((bb[0]+(bb[2]/2),bb[1]+(bb[3]/2))))

    # Get all the other color
    mult = 0.6
    dummy,im_diff_gray = cv.threshold(im[:,:,1],mult*255,255,cv.THRESH_BINARY)
    contours, heirarchy = cv.findContours(im_diff_gray,cv.RETR_EXTERNAL,cv.CHAIN_APPROX_NONE)
    for i in range(len(contours)):
        if cv.contourArea(contours[i])>1000:
            # Estimate the center
            bb = cv.boundingRect(contours[i])
            # Center of this object
            center = np.array((bb[0]+(bb[2])/2,bb[1]+(bb[3])/2))
            dist = 200.0 # Some random large number
            for j in range(len(req_centers)):
                if np.linalg.norm(center-req_centers[j])<dist:
                    dist = np.linalg.norm(center-req_centers[j])
            if dist>30.0:
                # Found a legitimate contour that was not discovered earlier
                req_contours.append(contours[i])
                req_centers.append(center)
    # Display the segments and get the features
    features = []
    for i in range(len(req_contours)):
        cv.drawContours(im_draw,req_contours[i],-1,(255,255,0),4)
        mask = np.zeros(im_diff_gray.shape,np.uint8)
        features.append(get_contour_feat(req_contours,i,mask,im_rgb))
    
    #cv.imshow('Segments from the image',im_draw)
    #cv.waitKey(-1)
    
    return (features,req_contours,req_centers,im_draw)


def get_contour_feat(contours,contour_id,mask,im_curr):
    # Approximate freeman chain for this contour
    cc,start = vf.freeman_chain(contours[contour_id])
    # Plot the contour obtained by freeman chain
    # plot_chain_code(cc,start,im_curr)
    # Obtain features to be used for classification
    coeff,a0,c0 = vf.fourier_approx(cc,10,100,True)
    # np.asarray(coeff).reshape(-1)
    # Getting the RGB values of pixels inside the contour
    cv.drawContours(mask,contours,contour_id,255,-1)
    av_rgb = cv.mean(im_curr,mask=mask)
    feat_val = np.zeros((coeff.shape[0]*coeff.shape[1]+3))
    feat_val[0:coeff.shape[0]*coeff.shape[1]] = np.asarray(coeff.reshape(-1))
    feat_val[-3:] = np.array([av_rgb[0]/255,av_rgb[1]/255,av_rgb[2]/255])
    return feat_val

def closest_shape(test_feat,features,contours,centers):
    n = 10
    min_center = 0
    dist_min = 100
    for i in range(len(features)):
        vis_feat = features[i]
        dist = np.min([utils.dist_metric(vis_feat[0:n],-test_feat[0:n]),utils.dist_metric(vis_feat[0:n],test_feat[0:n])])
        dist_rgb = utils.dist_metric(vis_feat[-3:]/sum(vis_feat[-3:]),test_feat[-3:]/sum(test_feat[-3:]))
        dist = dist+dist_rgb        # Check for smallest distance
        if dist<dist_min:
            dist_min = dist
            min_center = i
        print dist,centers[i],dist_rgb,dist-dist_rgb
    return min_center

if __name__ == "__main__":
    # Test image
    im = 'test_im_rgb.png'
    # Getting segments and features from the test image
    (features,contours,centers,im_draw) = segment_multiple(im)
    # Learning the dictionary
    # base directory where all the image and audio files are located
    base_dir = "/home/surenkum/work/vision_lang/data/data_shapes"
    if os.path.exists('./joint_dict.pickle'):
        # Load this data
        with open('joint_dict.pickle','r') as f:
            D1,D2,visual_mat = pickle.load(f)
    else:
        (D1,D2,visual_mat) = learn.learn_comp_dict(base_dir)
        # Writing this data to a pickle
        with open('joint_dict.pickle','w') as f:
            pickle.dump([D1,D2,visual_mat],f)

    # Get features for a test audio file
    #aud_file = '/home/surenkum/work/vision_lang/data/data_shapes/audio/green_square_6_rgb_color.wav.mfcc.csv'
    aud_file = '/home/surenkum/work/vision_lang/data/data_shapes/audio/yellow_halfcircle_2_rgb_color.wav.mfcc.csv'
    #aud_file = '/home/surenkum/work/vision_lang/data/data_shapes/audio/blue_triangle_2_rgb_color.wav.mfcc.csv'
    vis_feat = utils.single_test_audio_alt(aud_file,D1,D2,visual_mat[:-3,:].shape[0],visual_mat[-3:,:].shape[0],1,True)
    print vis_feat[-3:]
    min_center = closest_shape(vis_feat,features,contours,centers)
    print "Minimum center of shape is ",centers[min_center]
    # Displaying this data on image
    cv.circle(im_draw, tuple(centers[min_center]),10,(0,0,0),-1)
    cv.imshow('Segments from the image and required center',im_draw)
    cv.waitKey(-1)
    import pdb;pdb.set_trace()
