'''
..module:: shapes
:synopsis: This module is used to generate shape parameters
..moduleauthor:: Richard Doell <rfdoell@buffalo.edu>
'''
import numpy as np

def simple_triangle(center, scale):
	'''
	This method will return the coordinates of the points of an equilateral
	triangle with one point `up.' The center should be an (x, y) tuple
	indicating the center of the triangle and the scale should be a
	multiplicative constant which will resize the triangle from having unit
	distance between the center and points. This assumes that 0,0 is in the
	`top left' of the frame.
	:param center: The center of the triangle
	:type center: (int, int)
	:param scale: The scale factor to change the triangle by
	:type scale: real
	:rtype: [(int,int), (int, int), (int, int)]
	'''
	c_arr = np.array(center)
	#Define triangle as vector from center and scale them
	root3_2 = np.sqrt(3) / 2
	half = 0.5
	top_vec = np.array((0, -1)) * scale
	left_vec = np.array((-root3_2, half)) * scale
	right_vec = np.array((root3_2, half)) * scale

	#Now, generate the actual points:
	top_pt = tuple(np.round(top_vec + c_arr).astype(int))
	left_pt = tuple(np.round(left_vec + c_arr).astype(int))
	right_pt = tuple(np.round(right_vec + c_arr).astype(int))

	return [top_pt, left_pt, right_pt]

def simple_square(center, scale):
	'''
	This method will generate a set of coordinate points in the same system
	as get_simple_triangle. It uses a unit vector to the four points,
	assuming that the orientation of the square is such that the sides are
	normal to and parallel to the x and y axes (rather than a diamond). The
	center indicates the center of the square, the scale the relative
	sizing. Returns a pair of points indicating the bounds. Can also be
	used for circles.
	:param center: The center point of the square
	:type center: (int, int)
	:param scale: The size of the square
	:type scale: float
	:rtype: [(int, int), (int, int)]
	'''
	root2_2 = np.sqrt(2) / 2
	center = np.array(center)
	bot_right = tuple(np.array((root2_2, root2_2)) * scale + center)
	top_left = tuple(np.array((-root2_2, -root2_2)) * scale + center)
	return [top_left, bot_right]
