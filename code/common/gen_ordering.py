'''
.. module:: gen_ordering
:synopsis: This module will provide a way to generate an actual ordering of the\
compiled data for usage with training. It takes advantage of the fact that each\
of the dataset videos has a name of the following form: color_variant_speech in\
the large dataset. In the small dataset, the ordering isn't particularly\
important for the base case, as each color has a unique pronunciation. \
.. modulauthor:: Richard Doell <rfdoell@buffalo.edu>
'''

# TODO write this when we get to using the more complex set
