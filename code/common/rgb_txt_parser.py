'''
.. module:: rgb_txt_parser
::synopsis:: This module defines a parser for X11's rgb.txt file, which\
contains mappings of symbolic names to an RGB color space. This will be used to\
generate along with a text to speech program, a dataset of natural (spoken)\
language and color, to be learned jointly.\
.. moduleauthor:: Richard Doell <rfdoell@buffalo.edu>
'''

def parse_rgb_txt(rgb_txt_file):
	'''
	This method will parse the rgb.txt file (found in Ubuntu in
	/etc/X11/rgb.txt) for the colors and their mapping to an 8-bit RGB
	color space. It will create a dictionary mapping from rgb colors to a
	list containing the symbolic names for a color (which could have
	multiple names).
	:param rgb_txt_file: The opened file pointing to the rgb.txt file
	:type rgb_txt_file: file
	:rtype: dict mapping tuples of 8-bit integers to lists of strings
	'''
	xcol_dict = {}
	for line in rgb_txt_file:
		#Ignore comments/the first line
		if line[0] != '!':
			#First, parse out the line:
			line = line.strip()
			#Split on whitespace:
			parts = line.split()
			#Expect at least a 4-tuple. If not, throw an error:
			if len(parts) < 4:
				raise IOError('Error: expected 4 parts, got %d for line: %s'
				% (len(parts), line))
			#Expect the first three to be integers:
			#(This will error out if they're not)
			for i in range(3):
				int(parts[i])
			#Now, build a tuple from the first 3 elements:
			color = tuple([int(x) for x in parts[0:3]])
			#And the actual name string from the rest:
			name = ' '.join(parts[3:])
			#Check if it's in the list:
			if color not in xcol_dict:
				xcol_dict[color] = [name]
			else:
				xcol_dict[color].append(name)
	return xcol_dict

def test_parse_rgb_txt(path_to_rgb = '/etc/X11/rgb.txt'):
	'''
	This method will do a quick, likely platform-dependent test to see if
	the parsing method actually performs according to expectations. A
	random color will be tested for inclusion in the file, along with its
	full complement of symbolic names
	:param path_to_rgb: This is the path to the system's rgb.txt file
	:type path_to_rgb: str
	'''
	#Define the entry that we want to check:
	color_tuple = (153, 50, 204)
	color_names = ['dark orchid', 'DarkOrchid']
	#Open the appropriate file:
	rgb_file = open(path_to_rgb)
	#Parse it out:
	xcol_dict = parse_rgb_txt(rgb_file)
	#Check if our sample is in there, with the appropriate entries:
	test_passed = True
	if color_tuple in xcol_dict:
		name_list = xcol_dict[color_tuple]
		for item in color_names:
			if item not in name_list:
				test_passed = False
				break
		if test_passed:
			print 'PASSED - colors present'
		else:
			print 'FAILED - item %s not in dictionary' % item
	else:
		print 'FAILED - item %s not in dictionary' % (color_tuple,)

def main():
	'''
	This method will just run the test methods associated with this class
	'''
	test_parse_rgb_txt()

if __name__ == '__main__':
	main()
