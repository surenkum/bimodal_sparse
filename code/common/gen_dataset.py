'''
.. module:: gen_dataset
:synopsis: This module will generate an audio-visual dataset for use in the\
experiment. 
.. moduleauthor:: Richard Doell <rfdoell@buffalo.edu>
'''
import argparse
from espeak_wrapper import ESpeakWrapper
from rgb_txt_parser import parse_rgb_txt
import random
import os
import os.path
from PIL import Image
import uuid
import sh
import itertools
def main():
	'''
	This is the main method of this module, which handles setting up and
	running the dataset generation algorithm as well as parsing CLI input.
	'''
	#So, we should probably create a set of videos containing audio streams
	# and a one-pixel video stream containing the given color. Each of
	# these samples will be labeled by the actual symbolic name of the
	# color, then numbered. For example, we could have a
	# dark_orchid_000.mp4, which would be the first training sample
	# matching natural language to a color. This will require the
	# generation of the audio clip (with various parameters) and synthesis
	# with something like ffmpeg to actually create the video.
	
	#Command line options - number of samples per color name?
	parser = argparse.ArgumentParser(
		description =
		'This program will generate a dataset of short videos with colors and speech')
	parser.add_argument(
		'output_dir', nargs='?', default='./dataset', 
		help='The output directory for the dataset')
	args = parser.parse_args()
	if not os.path.isdir(args.output_dir):
		os.mkdir(args.output_dir)
	#TODO add arguments
	esw = ESpeakWrapper()
	xcol_dict = parse_rgb_txt(open('/etc/X11/rgb.txt'))
	avconv = sh.avconv
	color_count = 0
	for rgb_color in xcol_dict:
		names = xcol_dict[rgb_color]

		name_count = 0
		for name in names:
			base_name = os.path.join(args.output_dir, '%03d_%03d_'
					% (color_count, name_count))
			gen_data_variations(base_name, rgb_color, name)
			name_count += 1
		color_count += 1
def gen_data_variations(base_fpath, color, name):
	'''
	This method will handle creating variations in the voice which is used
	to describe the colors.
	:param base_fpath: This is the base file path which is used to then\
	generate new names
	:type base_fpath: str
	:param color: This is an RGB tuple describing the color
	:type color: (int, int, int)
	:param name: This is the symbolic name of the current color to\
	pronounce
	:type name: str
	'''
	#Create PIL image with the right color:
	curr_img = Image.new('RGB', (10,10), color)
	curr_img_name = '/dev/shm/%s.bmp' % uuid.uuid4() 
	curr_img.save(curr_img_name)

	#Create our instance of esw:
	esw = ESpeakWrapper()

	#assuming base_fpath of form /path/to/nnn_mmm_
	var_count = 0

	#vary pitch, pronunciation, speed and capitalization
	speeds = [100, 160, 220]
	pitches = [40, 80, 120]
	ampls = [8, 10, 12]
	cap_pitches = [1, 3, 50, 100]
	for speed, pitch, ampl, cap_pitch in itertools.product(speeds, pitches,
			ampls, cap_pitches):
		#generate our speech for this variation:
		esw.set_pitch(pitch)
		esw.set_wpm(speed)
		esw.set_ampl(ampl)
		esw.set_cap_pitch(cap_pitch)
		wav_file = esw.get_speech_wav(name)
		curr_out_name = base_fpath + '%03d.avi' % var_count
		synth_video(curr_img_name, wav_file, curr_out_name)

		#clean up:
		os.remove(wav_file)

		var_count += 1


	#Remove the temp color file:
	os.remove(curr_img_name)

def synth_video(img_name, wav_name, out_name):
	'''
	This method will create a video of the given name from the input image
	and sound
	:param img_name: This is the path to the image an the system
	:type img_name: str
	:param wav_name: This is the path to the audio file
	:type wav_name: str
	:param out_name: This is the output name to save as
	:type out_name: str
	'''
	sh.avconv('-loop', 1,
		'-shortest',
		'-y',
		'-i', img_name,
		'-i', wav_name,
		'-acodec', 'copy',
		'-vcodec', 'mjpeg',
		out_name)

if __name__ == '__main__':
	main()
