'''
.. module:: espeak_wrapper
:synopsis: This module provides a wrapper for the command line program, espeak.\
It does so in a fashion that allows for capture of the output wave data, so it\
can be then fed into numpy arrays for analysis.\
.. moduleauthor:: Richard Doell <rfdoell@buffalo.edu>
'''
import sh
#from StringIO import StringIO
#from cStringIO import StringIO
#from io import BytesIO
import scipy.io.wavfile
#for kludge
import uuid
import os

class ESpeakWrapper(object):
	'''
	This class defines a wrapper for the espeak program, accomplished with
	the use of sh. It accepts initialization parameters as would be passed
	to espeak through the command line. It will create numpy arrays
	representing `speech' generated from a given word. Parameters may be
	edited after initialization.

	:param ampl: This is the amplitude of the voice. For audible output, it\
	should range from 1-20
	:type ampl: int
	:param pitch: How high or low pitched the voice should be (0-99)
	:type ampl: int
	:param wpm: The speed of the voice in words per minute
	:type wpm: int
	:param voice: The voice to use (found in the espeak-data/voices folder)
	:type voice: str
	:param cap_pitch: How to pitch capital letters. Default is no change,\
	higher numbers mean to inflect capitals with a higher pitch (3-100)
	:type cap_pitch: int
	'''
	def __init__(self, ampl=10, pitch=80, wpm=160, voice='en-us',
			cap_pitch=1):
		self.ampl = ampl
		self.pitch = pitch
		self.wpm = wpm
		self.voice = voice
		self.cap_pitch = cap_pitch
		#self.espeak = sh.espeak

	def get_speech_arr(self, phrase):
		'''
		This method will return a tuple consisting of the read data
		rate as well as the numpy array containing the data.
		:param phrase: A period delimited block of text to be spoken.\
		No quotes or newlines should be here
		:type phrase: str
		:rtype: (int, numpy.ndarray)
		'''
		#This is now tied to the kludgy method, because this will be
		# used to generate the dataset, not to evaluate it
		fpath = self.get_speech_wav(phrase)
		rate, data = scipy.io.wavfile.read(fpath)
		os.remove(fpath)
		return (rate, data)

	def get_speech_wav(self, phrase):
		'''
		This method will create a wav file in /dev/shm which contains
		the spoken version of the given phrase. It will return the path
		to the file.
		:param phrase: A period delimited block of text to be spoken.\
		No quotes or newlines should be here
		:type phrase: str
		:rtype: str
		'''
		fpath = '/dev/shm/%s' % uuid.uuid4()
		data = sh.espeak('-a', '%d' % self.ampl,
			'-p', '%d' % self.pitch,
			'-s', '%d' % self.wpm,
			'-v', self.voice,
			'-k', self.cap_pitch,
			'"%s"' % phrase,
			'-w', fpath)
		return fpath

	def set_pitch(self, pitch):
		'''
		This method changes the internally configured pitch
		:param pitch: How high or low pitched the voice should be (0-99)
		:type pitch: int
		'''
		self.pitch = pitch

	def set_ampl(self, ampl):
		'''
		This method changes the internally configured amplitude
		:param ampl: How loud or soft the voice should be (1-20)
		:type ampl: int
		'''
		self.ampl = ampl

	def set_wpm(self, wpm):
		'''
		This method changes the internally configured wpm
		:param wpm: How fast or slow the voice should be (10-200)
		:type wpm: int
		'''
		self.wpm = wpm

	def set_voice(self, voice):
		'''
		This method changes the internally configured voice
		:param voice: Which voice should be used (in espeak-data/voices)
		:type voice: str
		'''
		self.voice = voice

	def set_cap_pitch(self, cap_pitch):
		'''
		This method changes the internally configured capitalization
		pitch
		:param cap_pitch: What the emphasis on captialization should be\
		(3-100 for higher pitched, 1 for no emphasis)
		:type cap_pitch: int
		'''
		self.cap_pitch = cap_pitch

	def set_defaults(self):
		'''
		This method will reset the voice synthesizer to its defaults
		'''
		ampl=10
		pitch=80
		wpm=160
		voice='en-us'
		cap_pitch=1
		self.ampl = ampl
		self.pitch = pitch
		self.wpm = wpm
		self.voice = voice
		self.cap_pitch = cap_pitch

def main():
	'''
	This method will create a default instance of the wrapper and check to
	see if it can generate output
	'''
	esw = ESpeakWrapper()
	rate, data = esw.get_speech('Hello, there. How are you today?')
	scipy.io.wavfile.write('test.wav', rate, data)

if __name__ == '__main__':
	main()
