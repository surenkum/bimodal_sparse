'''
This file evaluated the quantitative accuracy of ranking algorithm
'''
import cPickle
import string
import numpy as np
import matplotlib.pyplot as plt
import os
import sys
from utils import datadir

def matplotlibusetex():
    from matplotlib import rc
    #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
    ## for Palatino and other serif fonts use:
    rc('font',**{'family':'serif','serif':['Times']})
    rc('text', usetex=True) 

def get_specific(c,im,color=[],shape=[]):
    '''
    Extact color and shape specific information from c metric
    '''
    # Go over k folds
    data = []
    for i in range(len(im)):
        single_list = im[i]
        single_c = c[i]
        for j in range(len(single_list)):
            # Going over individual file name
            fname = single_list[j]
            # Split the file name
            sp = string.split(fname,'_')
            if not color:
                # Color empty  - nothing to do
                pass
            else:
                if sp[0] == color:
                    data.append(single_c[j])
            # Check for shape
            if not shape:
                # shape empty  - nothing to do
                pass
            else:
                if sp[1] == shape:
                    data.append(single_c[j])
    return np.array(data)                    

'''
To evaluate the number of points having rank less than k
input: ranks - array
'''

def gen_rank(ranks,k=5):
    acc = sum(ranks<=k)
    return acc


def script_directory():
    return (os.path.dirname((__file__ or sys.argv[0])) or '.')

def gen_plots(c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single):
    '''
    Generates the plots when called with relevant data
    '''
    matplotlibusetex()
    # Evaluate for various colors and shapes
    colors  = ['blue','red','green','yellow']
    shapes = ['rectangle','square','circle','halfcircle','rhombus','trapezium','hexagon','triangle']
    colors_single = np.zeros((len(colors)))
    colors_comp = np.zeros((len(colors)))
    shapes_single = np.zeros((len(shapes)))
    shapes_comp = np.zeros((len(shapes)))
    i = 0
    for color in colors:
        data = get_specific(c_comp,im_comp,color)
        colors_comp[i] = gen_rank(data)
        data = get_specific(c_single,im_single,color)
        colors_single[i] = gen_rank(data)
        i = i+1
    i = 0
    for shape in shapes:
        data = get_specific(s_comp,im_comp,[],shape)
        shapes_comp[i] = gen_rank(data)
        data = get_specific(s_single,im_single,[],shape)
        shapes_single[i] = gen_rank(data)
        i = i+1

    # Some actual plotting
    # Shape plot
    ind = np.arange(len(shapes))
    width = 0.35

    fig = plt.figure(figsize=(6, 2.5))
    ax = fig.add_subplot(121)
    fig.subplots_adjust(bottom=0.30, right=0.95, top=0.93, left=.10)
    rects1 = ax.bar(ind,shapes_comp,width,color='r')
    rects2 = ax.bar(ind+width,shapes_single,width,color='b')
    ax.set_xlabel('Shapes')
    ax.set_ylabel('Correct Retrievals')
    #ax.set_title('Scores by group and gender')
    ax.set_xticks(ind+width)
    ax.set_xticklabels( shapes , rotation=70)
    leg = ax.legend( (rects1[0], rects2[0]), ('Compositional', 'Paired') ,
            prop={'size' : 12})
    #leg.get_frame().set_alpha(0.1)
    plt.savefig(script_directory() + '/' +
            '../figures/shape-accuracy.pdf')

    ind = np.arange(len(colors))
    width = 0.45
    #fig = plt.figure(figsize=(3, 2.5))
    ax = fig.add_subplot(122)
    #fig.subplots_adjust(bottom=0.15, right=0.95, top=0.95, left=.15)
    rects1 = ax.bar(ind,colors_comp,width,color='r')
    rects2 = ax.bar(ind+width,colors_single,width,color='b')
    #ax.set_ylabel('Number of Correct Retrievals')
    #ax.set_title('Scores by group and gender')
    ax.set_xticks(ind+width)
    ax.set_xticklabels( colors , rotation=70)
    #ax.legend( (rects1[0], rects2[0]), ('Compositional', 'Paired') )
    plt.savefig(script_directory() + '/' +
            '../figures/color-accuracy.pdf')
    plt.show()


if __name__ == '__main__':
    # Load the data
    (c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single) = \
            cPickle.load(open(datadir("allmydata.pickle")))
    gen_plots(c_comp,s_comp,a_comp,im_comp,c_single,s_single,a_single,im_single)
    
