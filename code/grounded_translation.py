'''
This function attempts to generate machine translation between the audio samles of two languages
based on their distance in visual space
Idea: Use learned compositional dictionary for both the languages and then
1) Get lingual structure alignment by multiplying H matrices for both the languages
2) Cluster on audio segments
3) Align the audio clusters using a hungarian algorithm by measuring the distance in vision space
'''

import comp_dict_learn as main_alt # This is to learn compositional dictionaries on both languages


if __name__ == '__main__':


